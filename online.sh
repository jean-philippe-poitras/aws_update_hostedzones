#!/usr/bin/env bash

aws route53 change-resource-record-sets --hosted-zone-id Z2BVV8IGTV5A92 --change-batch file://switch_foo/online_recordsets/atelierduchef.centrerockland.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z4IQPC1TZ69GE --change-batch file://switch_foo/online_recordsets/concours.carrefourfrontenac.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/espacebureau.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/espace.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/espacedetail.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/espaceindustriel.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/espaces.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z3073YD0EFVUD4 --change-batch file://switch_foo/online_recordsets/foretdetoilescominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z24BVGK3MHAN7M --change-batch file://switch_foo/online_recordsets/garecentrale.ca.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/intranet.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z74FUZ64E9SN7 --change-batch file://switch_foo/online_recordsets/lesgaleriesdehull.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/officespace.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z4IQPC1TZ69GE --change-batch file://switch_foo/online_recordsets/porte-cles-chanceux.carrefourfrontenac.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/sac-web.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/sondages.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/tourneefmd.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/www.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/www.espace.cominar.com.online
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/online_recordsets/www.espaces.cominar.com.online
