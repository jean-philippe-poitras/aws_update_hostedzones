#!/usr/bin/env bash

current_date=$(date +%Y-%m-%d.%H-%M-%S)
working_dir="switch_foo"
distributions_dir="$working_dir/cloudfront_distributions"
online_recordsets="$working_dir/online_recordsets"
offline_recordsets="$working_dir/offline_recordsets"

mkdir -p "$working_dir"
mkdir -p "$distributions_dir"
mkdir -p "$online_recordsets"
mkdir -p "$offline_recordsets"

echo "Getting distributions"
cloud_distribution_ids=$(aws cloudfront list-distributions | jq --raw-output ".DistributionList.Items[].Id")
echo "Done getting distributions"

echo "Getting distributions config"

for dist_id in $cloud_distribution_ids
do
	if [ ! -f "$distributions_dir/${dist_id}.dist" ];
	then
		echo "Getting ${dist_id} config"
		aws cloudfront get-distribution-config --id ${dist_id} | jq --raw-output ".DistributionConfig.Aliases.Items[]" > "$distributions_dir/${dist_id}.dist"
	else
		echo "${dist_id}.dist exists, skipping"
	fi
done
echo "Done getting configs"

offline_json_template="{ \"Comment\": \"Update the A record set\", \"Changes\":  [ { \"Action\": \"UPSERT\", \"ResourceRecordSet\": { \"Name\": \"%s\", \"Type\": \"A\", \"AliasTarget\": { \"HostedZoneId\": \"%s\", \"DNSName\": \"%s\", \"EvaluateTargetHealth\": false } } } ] }"
online_json_template="{ \"Comment\": \"Update the A record set\", \"Changes\": [ { \"Action\": \"UPSERT\", \"ResourceRecordSet\": %s } ] }"

while read domain;
do
	echo "Backuping current recordset for ${domain}"
	for hosted_zone_id in $(aws route53 list-hosted-zones|jq --raw-output ".HostedZones[].Id")
	do 
		if [ ! -f "$online_recordsets/${domain}.online" ];
		then
			record_set=$(aws route53 list-resource-record-sets --hosted-zone-id ${hosted_zone_id/\/hostedzone\//} --query "ResourceRecordSets[?Name == '${domain}.' && Type == 'A']" | jq --raw-output ".[]")

			if [ -n "$record_set" ]; then
			    echo "$record_set"	
			    echo $(printf "${online_json_template}" "${record_set}") >> "$online_recordsets/${domain}.online"
		            echo "Done backing up ${domain}"

			    echo "Preparing CloudFront Distribution recordset for ${domain}"
			    distribution=$(grep -lP "^${domain}$" ${distributions_dir}/*.dist)
				echo "${distribution}"
			    distribution=$(basename "${distribution}")
				
			    dist_domain=$(aws cloudfront get-distribution --id ${distribution//.dist/} | jq --raw-output ".Distribution.DomainName")

			    # Z2FDTNDATAQYW2 -- hardcoded value @see https://docs.aws.amazon.com/fr_fr/AWSCloudFormation/latest/UserGuide/aws-properties-route53-aliastarget.html#cfn-route53-aliastarget-hostedzoneid 
			    echo $(printf "${offline_json_template}" "${domain}." "Z2FDTNDATAQYW2" "${dist_domain}") > "$offline_recordsets/${domain}.offline"
			fi
		fi
	done
done < $1 
