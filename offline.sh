#!/usr/bin/env bash

aws route53 change-resource-record-sets --hosted-zone-id Z2BVV8IGTV5A92 --change-batch file://switch_foo/offline_recordsets/atelierduchef.centrerockland.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z4IQPC1TZ69GE --change-batch file://switch_foo/offline_recordsets/concours.carrefourfrontenac.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/espacebureau.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/espace.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/espacedetail.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/espaceindustriel.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/espaces.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z3073YD0EFVUD4 --change-batch file://switch_foo/offline_recordsets/foretdetoilescominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z24BVGK3MHAN7M --change-batch file://switch_foo/offline_recordsets/garecentrale.ca.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/intranet.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z74FUZ64E9SN7 --change-batch file://switch_foo/offline_recordsets/lesgaleriesdehull.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/officespace.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z4IQPC1TZ69GE --change-batch file://switch_foo/offline_recordsets/porte-cles-chanceux.carrefourfrontenac.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/sac-web.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/sondages.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/tourneefmd.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/www.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/www.espace.cominar.com.offline
aws route53 change-resource-record-sets --hosted-zone-id Z1CBZYNIDKR96C --change-batch file://switch_foo/offline_recordsets/www.espaces.cominar.com.offline
